<?php


 include "conexion.php";

 if(empty($_GET['id'])){
     header('Location: ofertas.php');
 }

 $id_empleo = $_GET['id'];

 $sql= mysqli_query($conection, "SELECT nombre_puesto FROM EMPLEO WHERE ID_EMPLEO=$id_empleo");

  $result= mysqli_num_rows($sql);
  if($result=0){
    header('Location: ofertas.php');   
  }else{
      while($data= mysqli_fetch_array($sql)){
          $nom_puesto= $data['nombre_puesto'];
      }
  }
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&display=swap" rel="stylesheet"> 

    <link rel="stylesheet" type="text/css" href="sistema/css/stylepostulacion.css">
	
	<title>Empleos Informales</title>
</head>
<body>
<header>
<div class="header">
			<div class="optionsBar">
            <h1>Empleos Informales</h1>
            </div>
				
            <nav>
                <ul>
               
                <li class="first"><a href="contacto.php">Contacto</a></li>
                <li class="first"><a href="publicar.php">Publicar Empleo</a></li>
               
                <li class="first">
					<a href="ofertas.php">Ofertas</a>
				</li>
                <li class="first"><a href="index.php">Inicio</a></li>
				
                </ul>
           
        </nav>

		</div>
    </header>
    <main>
        <section class="contenedor sobre-nosotros">
            <center>
            <h2  style="background:#FFFF; width:600px; text-align:center; border-radius:10px;" class="titulo">Registra tu informacion</h2>
           
            </center>
            <div class="contenedor-sobre-nosotros">
                <div class="contenido-textos">
                <form class="formulario"   style="width: 650px; height:670px; text-align:left; " method="POST" action="registrarPostulacion.php" onsubmit="return confirm('Are you sure you want to submit?');">
                
                <input style="margin-bottom:2px; margin-left:20px; border-radius:5px;" name="primer_nombre" required placeholder="Primer Nombre"> 
           
            <input  style="margin-left:20px; border-radius:5px;" name="segundo_nombre" required placeholder="Segundo Nombre"> 
           
            <input  style="margin-left:20px; border-radius:5px;" name="primer_apellido" required placeholder="Primer Apellido"> 
            
            <input  style="margin-left:20px; border-radius:5px;" name="segundo_apellido" required placeholder="Segundo apellido"> 
           
            <input style="margin-left:150px; border-radius:5px;"  name="telefono" type="telefono" required placeholder="Telefono"> <br>
         
                
               <input  style="margin-left:150px; border-radius:5px;" name="puesto"  style="font-size:18px; color:#0a062e; border-radius:10px;"  disabled  value="<?php echo $nom_puesto;?>" >  <br>
               
            
            <input style="margin-left:150px; border-radius:5px;" name="direccion" type="text" required placeholder="Direccion"> <br>
            
            
            <textarea style="margin-left:140px;" name="mensaje" required="" placeholder="Escribe tu mensaje"></textarea> <br>
            <center>
            <input style=" margin-right:80px; height:40px; width:100px; background-color: brown; font-size:16px; color:#FFFF; border-radius:5px;"type="submit" name="Registrar" value="Enviar" >
            <input style="  margin-left:80px; height:40px; width:150px; background-color: brown; font-size:16px; color:#FFFF; border-radius:5px;"type="submit" name="Registrar" value="Regresar" href="ofertas.php">
           
            </center>
            </form>
            
                </div>
            </div>
        </section>
        
        <section class="about-services">
            <div class="contenedor">
                <h2 class="titulo">Nuestros servicios</h2>
                <div class="servicio-cont">
                    <div class="servicio-ind">
                    <img src="img/publi.png" alt="">
                        <h3>Publica tu empleo</h3>
                        <p>Si deseas publicar un empleo visita el modulo de "publicar empleo" en donde podras encontrar información
                            necesaria para poder hacer la publicación?</p>
                    </div>
                    <div class="servicio-ind">
                    <img src="img/nus1.png" alt="">
                        <h3>Consigue empleo</h3>
                        <p>¿Buscas empleo? No lo pienses mas! explora las distintas ofertas publicadas en nuestro sitio</p>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <footer>
        <div class="contenedor-footer">
            <div class="content-foo">
                <h4>Contacto</h4>
                <p>51156211</p>
            </div>
            <div class="content-foo">
                <h4>Email</h4>
                <p>empleosinformales@gmail.com</p>
            </div>
            <div class="content-foo">
                <h4>Direccion</h4>
                <p>San José Chicalquix, Sija</p>
            </div>
        </div>
        <h2 class="titulo-final">&copy; Empleos Informales | Portal empleo</h2>
    </footer>
</body>

</html>
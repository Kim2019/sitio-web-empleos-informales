

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&display=swap" rel="stylesheet"> 

    <link rel="stylesheet" type="text/css" href="sistema/css/stylecontacto.css">
	
	<title>Empleos Informales</title>
</head>
<body>
<header>
<div class="header">
			<div class="optionsBar">
            <h1>Empleos Informales</h1>
            </div>
				
            <nav>
                <ul>
               
                <li class="first"><a href="contacto.php">Contacto</a></li>
                <li class="first"><a href="publicar.php">Publicar Empleo</a></li>
               
                <li class="first">
					<a href="ofertasP.php">Ofertas</a>
				</li>
                <li class="first"><a href="index.php">Inicio</a></li>
				
                </ul>
           
        </nav>

		</div>
    </header>
    <main>
        <section class="contenedor sobre-nosotros">
            <h2 class="titulo">CONTACTANOS</h2>
            <div class="contenedor-sobre-nosotros">
                <div class="contenido-textos">
                <form action="registrarContacto.php"  method="post" class="formulario" >
            <label >Nombre:</label>
            <input  name="nombre" required placeholder="Nombre completo">
            <label >Email:</label>
            <input name="email" type="email" required placeholder="ejemplo@email.com">
            <label >Mensaje:</label>
            <textarea name="mensaje" required="" placeholder="Escribe tu mensaje"></textarea>
            <input style="  background-color: brown; font-size:18px; color:#FFFF; border-radius:10px;" type="submit" name="Registrar" value="Enviar">
           </form>
            
                </div>
                
            </div>
        </section>
        
        <section class="about-services">
            <div class="contenedor">
                <h2 class="titulo">Nuestros servicios</h2>
                <div class="servicio-cont">
                    <div class="servicio-ind">
                    <img src="img/publi.png" alt="">
                        <h3>Publica tu empleo</h3>
                        <p>Si deseas publicar un empleo visita el modulo de "publicar empleo" en donde podras encontrar información
                            necesaria para poder hacer la publicación?</p>
                    </div>
                    <div class="servicio-ind">
                    <img src="img/nus1.png" alt="">
                        <h3>Consigue empleo</h3>
                        <p>¿Buscas empleo? No lo pienses mas! explora las distintas ofertas publicadas en nuestro sitio</p>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <footer>
        <div class="contenedor-footer">
            <div class="content-foo">
                <h4>Contacto</h4>
                <p>51156211</p>
            </div>
            <div class="content-foo">
                <h4>Email</h4>
                <p>empleosinformales@gmail.com</p>
            </div>
            <div class="content-foo">
                <h4>Direccion</h4>
                <p>San José Chicalquix, Sija</p>
            </div>
        </div>
        <h2 class="titulo-final">&copy; Empleos Informales | Portal empleo</h2>
    </footer>
</body>

</html>
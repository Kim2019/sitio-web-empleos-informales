<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="sistema/css/styleofertas.css">
	<title>Empleos Informales</title>
</head>
<body>

	<header >
		<div class="header">
			
        <h1>Empleos Informales</h1>

            <nav>
                <ul>
                <li class="first"><a href="contacto.php">Contacto</a></li>
                <li class="first"><a href="publicar.php">Publicar Empleo</a></li>
                
				<li class="first">
					<a href="ofertas.php">Ofertas</a>
				</li>
                <li class="first"><a href="index.php">Inicio</a></li>
                </ul>
           
        </nav>
			
        </div>
        <section class="contenedor sobre-nosotros">
           <div class="title">
           <center>
           <strong><h1  style="font-size: 40px; color:#06062b; margin:2; border-radius:10px; background-color:#ebe9e7ee; width:600px">Ofertas Disponibles</h1></strong>
           </center>
           </div>
            <div class="contenedor-sobre-nosotros">
                <div class="contenido-textos">
        <?php
          include "conexion.php"; 
        
    $sql="SELECT A.id_empleo,A.imagen, A.nombre_puesto,B.nombre_empresa,B.direccion,A.horarios,A.requisitos,A.detalles 
    from empleo A 
    inner join empresa B on A.empresa=B.id_empresa 
    inner join categoria C on a.categoria=c.id_categoria
    where C.tipo_categoria='Albañileria'";

    $rs = mysqli_query($conection,$sql);
	$row = mysqli_fetch_assoc($rs);
	$total_rows = mysqli_num_rows($rs);
   ?>
    <?php do { ?>
        <div class="empleos" style="width: 33.33%;">
    
        <img src="<?php echo $row['imagen'];?>" height="130px" width="260px" aling="center"/><br>
  
    
    <center><h2 style="font-size: 22px; font-family:Copperplate Gothic; "><?php echo($row['nombre_puesto']);?> <br></h2> 
	</center>
    <center>
            <h4 style="font-size:28px; font-family:Brush Script MT;"><?php echo($row['nombre_empresa']);?></h4> 
    </center>
    <h4 style="font-size:17px; font-family:Copperplate;">
        <strong>No.Empleo:</strong> <?php echo($row['id_empleo']);?> 
    </h4>
	<h4 style="font-size:17px; font-family:Copperplate;">
        <strong>Dirección:</strong> <?php echo($row['direccion']);?> 
    </h4>
    <h4 style="font-size:17px; font-family:Copperplate;">
        <strong>Horarios:</strong><?php echo($row['horarios']);?> 
    </h4>
    <h4 style="font-size:17px; font-family:Copperplate;">
       <strong>
       Requisitos: 
       </strong>  
       <?php echo($row['requisitos']);?>
    </h4>
   <h4 style="font-size:17px; font-family:Copperplate;">
        <strong>Detalles:</strong>
        <?php echo($row['detalles']);?> <hr>
   </h4>
	
    
    <center><a aling="center" class="btn_post" href="postulacion.php?id=<?php echo($row['id_empleo']);?>">Postularme</a>
</center>
	</div> 


<?php } while ($row = mysqli_fetch_assoc($rs)); ?>
 
</div>
            </div>
        </section>
	</header>
<main>
        
        <section class="about-services">
            <div class="contenedor">
               
        <form class="form-busqueda" action="buscar.php" method="get" target="_blank">
        <h2>Buscar</h2>        
        <input type="search" id="nombre_empresa" name="nombre_puesto" value="" placeholder="empleo, empresa, direccion"> <br> <button>Enviar</button>
        </form>
            </div>
        </section>
    </main>
    <footer>
        <div class="contenedor-footer">
            <div class="content-foo">
                <h4>Contacto</h4>
                <p>51156211</p>
            </div>
            <div class="content-foo">
                <h4>Email</h4>
                <p>empleosinformales@gmail.com</p>
            </div>
            <div class="content-foo">
                <h4>Direccion</h4>
                <p>San Carlos Sija,Quetgo</p>
            </div>
        </div>
        <h2 class="titulo-final">&copy; Empleos Informales | Portal empleo</h2>
    </footer>
</body>

</html>


  
    
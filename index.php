


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&display=swap" rel="stylesheet"> 

    <?php include "sistema/scripts.php"; ?>
	<title>Empleos Informales</title>
</head>
<body>
<header>
<div class="header">
			<div class="optionsBar">
            <h1>Empleos Informales</h1>
            </div>
           <div class="optionsBar">
            <h2 aling="left">Guatemala, <?php echo fechaC(); ?></h2>

            </div>
				
            <nav>
                <ul>
                <li class="first"><a href="contacto.php">Contacto</a></li>
                <li class="first"><a href="publicar.php">Publicar Empleo</a></li>
                <li class="first">
					<a href="ofertas.php">Ofertas</a>
					<ul class="content1">
						<li class="second"><a href="#">Categorías</a>
                        <ul class="content2">
                            
                            
                            <li><a class="tird" href="CatPanaderia.php">Panadería</a><br>
                            <li><a class="tird" href="albañileria.php">Albañileria</a><br>
                            
                          </ul>

                         </li>
					</ul>
				</li>
                <li class="first"><a href="index.php">Inicio</a></li>
				
                </ul>
        </nav>

		</div>
        
        <section class="textos-header">
            <h1>Bienvenido al portal de empleos informales!!!</h1>
            <hr style="width:700px; background:#FFFF; height:3px;"> <hr>
            <h2>Consigue empleo YA!!</h2>
        </section>
    </header>
    <main>
        <section class="contenedor sobre-nosotros">
            <h2 class="titulo">Acerca de nosotros</h2>
            <div class="contenedor-sobre-nosotros">
                <div class="contenido-textos">
                    <p> "Empleos informales" es un pagina que te muestra informacion
                    acerca de diferentes empleos disponibles enfocado con el objetivo
                    de facilitar a los jóvenes sin experiencia, o sin estudios la oportunidad 
                    de optar por un empleo de acuerdo a sus capacidades mejorando así su desarrollo. </p>
                </div>
            </div>
        </section>
        
        <section class="about-services">
            <div class="contenedor">
                <h2 class="titulo">Nuestros servicios</h2>
                <div class="servicio-cont">
                    <div class="servicio-ind">
                        <img src="img/publi.png" alt="">
                        <h3>Publica tu empleo</h3>
                        <p>Si deseas publicar un empleo visita el modulo de "publicar empleo" en donde podras encontrar información
                            necesaria para poder hacer la publicación?</p>
                    </div>
                    <div class="servicio-ind">
                        <img src="img/nus1.png" alt="">
                        <h3>Consigue empleo</h3>
                        <p>¿Buscas empleo? No lo pienses mas! explora las distintas ofertas publicadas en nuestro sitio</p>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <footer>
        <div class="contenedor-footer">
            <div class="content-foo">
                <h4>Contacto</h4>
                <p>51156211</p>
            </div>
            <div class="content-foo">
                <h4>Email</h4>
                <p>empleosinformales@gmail.com</p>
            </div>
            <div class="content-foo">
                <h4>Direccion</h4>
                <p>San José Chicalquix, Sija</p>
            </div>
        </div>
        <h2 class="titulo-final">&copy; Empleos Informales | Portal empleo</h2>
    </footer>
</body>

</html>
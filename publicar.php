<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&display=swap" rel="stylesheet"> 

    <?php include "sistema/scripts.php"; ?>
	<title>Empleos Informales</title>
</head>
<body>
<header>
<div class="header">
			<div class="optionsBar">
            <h1>Empleos Informales</h1>
            </div>
           
            <nav>
                <ul>
  
                <li class="first"><a href="contacto.php">Contacto</a></li>
                <li class="first"><a href="publicar.php">Publicar Empleo</a></li>
               
                <li class="first">
					<a href="ofertas.php">Ofertas</a>
				</li>
                <li class="first"><a href="index.php">Inicio</a></li>
				
                </ul>
           
        </nav>

		</div>
        
        <section class="contenedor sobre-nosotros">
           <div class="title">
           
           </div>
            <div class="contenedor-sobre-nosotros">
                <div class="contenido-textos" 
                style="font-size:20px; color:#06062b; margin:2; border-radius:10px; background-color:#755252; width:800px">
                <center>
           <strong><h1 style="font-size:30px; color:#FFFF; margin:5px margin-bottom=5px" >Publicar ofertas de trabajo</h1></strong>
           </center>
                <center>
               <p style="margin-top:15px; color:#FFFF;"> Si desea publicar una oferta de trabajo en este sitio web deberá enviar
                     la siguiente información sobre la oferta disponible en su empresa</p>

               </center> 
                    <p style="font-size:20px; color:#FFFF;">- Nombre de la empresa</p>
                   <p style="font-size:20px; color:#FFFF;"> - Nombre del puesto</p>
                   <p style="font-size:20px; color:#FFFF;"> - Nombre del contacto</p>
                   <p style="font-size:20px; color:#FFFF;"> - Correo, número de teléfono y direccion del contacto</p>
                   <p style="font-size:20px; color:#FFFF;"> - Requisitos de la vacante</p>
                   <p style="font-size:20px; color:#FFFF;"> - Horarios</p>
                   <p style="font-size:20px; color:#FFFF;"> - Detalles</p>
                   <strong><h1 style="font-size:20px; color:#FFFF; margin:10px; margin-bottom:15px;" >Toda la información la deberá enviar a empleosinformales@gmail.com</h1></strong>
           
                </div>
            </div>
        </section>
    </header>
    <main>
        
        
    <section class="about-services">
            <div class="contenedor">
                <h2 class="titulo">Nuestros servicios</h2>
                <div class="servicio-cont">
                    <div class="servicio-ind">
                    <img src="img/publi.png" alt="">
                        <h3>Publica tu empleo</h3>
                        <p>Si deseas publicar un empleo visita el modulo de "publicar empleo" en donde podras encontrar información
                            necesaria para poder hacer la publicación?</p>
                    </div>
                    <div class="servicio-ind">
                    <img src="img/nus1.png" alt="">
                        <h3>Consigue empleo</h3>
                        <p>¿Buscas empleo? No lo pienses mas! explora las distintas ofertas publicadas en nuestro sitio</p>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <footer>
        <div class="contenedor-footer">
            <div class="content-foo">
                <h4>Contacto</h4>
                <p>51156211</p>
            </div>
            <div class="content-foo">
                <h4>Email</h4>
                <p>empleosinformales@gmail.com</p>
            </div>
            <div class="content-foo">
                <h4>Direccion</h4>
                <p>San José Chicalquix, Sija</p>
            </div>
        </div>
        <h2 class="titulo-final">&copy; Empleos Informales | Portal empleo</h2>
    </footer>
</body>

</html>